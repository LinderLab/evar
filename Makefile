
usage:
	@echo 'usage:'
	@echo '    1: docker run --rm -v "$(pwd)/data:/export" pradosj/evar Makefile.config'
	@echo '    2: Edit parameters into the newly created Makefile.config'
	@echo '    3: Run the pipeline'
	@exit 2
  
-include Makefile.config
include /tmp/jf/Makefile.jf
include /tmp/evar/Makefile.evar 


Makefile.config:
	cp /tmp/Makefile.config $@
	