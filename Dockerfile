FROM pradosj/ngs


# install R
RUN apt-get update && apt-get install -y apt-transport-https software-properties-common
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9
RUN add-apt-repository 'deb [arch=amd64,i386] https://cran.rstudio.com/bin/linux/ubuntu xenial/'
RUN apt-get update && apt-get install -y r-base libcurl4-openssl-dev libssl-dev libmariadb-client-lgpl-dev libxml2-dev pandoc

# install R packages
RUN Rscript -e 'install.packages(c("ggplot2","devtools","igraph","visNetwork"),repos="https://stat.ethz.ch/CRAN/")'
RUN Rscript -e 'source("https://bioconductor.org/biocLite.R");biocLite(c("rtracklayer","Rsamtools","GenomicAlignments","GenomicFeatures"))'

# install pigz
RUN apt-get update && apt-get install -y pigz


ADD evar/ /tmp/evar
ADD Makefile Makefile.config /tmp/

ADD data/ /tmp/data
RUN make -f /tmp/evar/Makefile.evar /tmp/data/SA564.fa.pac


ADD jf/ /tmp/jf
RUN cd /tmp/jf && g++ -O3 -std=c++14 -I/usr/local/include/jellyfish-2.3.0 kmer_count.cpp -L/usr/local/lib -ljellyfish-2.0 -lpthread -o kmer_count
ENV LD_LIBRARY_PATH /usr/local/lib





WORKDIR /export
VOLUME ["/export/"]
ENTRYPOINT ["make", "-f", "/tmp/Makefile"]



